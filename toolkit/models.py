import tensorflow as tf
import numpy as np
import pandas as pd

from sklearn import ensemble, cluster, naive_bayes, neighbors


def create_ann(
    num_nodes_0,
    num_nodes_1,
    optimizer="adam",
    loss="sparse_categorical_crossentropy",
    metrics=["accuracy"],
):
    """Create an artificial neural network that will learn to identify
    handwritten digits.

    Keras : The Three Modules
    Models - Sequential or Functional
    Layers - Dense || Conv2D || Reccurent
    Core - visualization || plotting

    args:
        num_nodes_0: The number of "neurons" to put in the first layer
        num_nodes_1: The number of "neurons" to put in the second hidden layer

    return:
        model

    """
    # Artificial Nerual Network Instantiation
    model = tf.keras.models.Sequential()
    # Add a flat layer
    model.add(tf.keras.layers.Flatten())
    # Build the input and the hidden layers
    model.add(tf.keras.layers.Dense(num_nodes_0, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(num_nodes_1, activation=tf.nn.relu))
    # Build the output layer. NOTE These nodes correspond to 10 digits.
    model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))
    # Model Compilation
    model.compile(
        optimizer="adam", loss="sparse_categorical_crossentropy", metrics=["accuracy"]
    )
    return model
