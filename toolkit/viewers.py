import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np


def view_digits(x_train: pd.DataFrame, y_train: pd.DataFrame):
    """View the handwritten digits
    args:
        x_train : pd.DataFrame # Features dataset
        y_train : pd.Series    # Targets

    returns:
        ( fig: plt.Figure, axes: [plt.Axes] )
    """
    # figure and axes object
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(7, 7))

    axes = [ax.imshow(x_train[i], cmap="gray") for i, ax in enumerate(axes.flatten())]

    fig.suptitle("Viewing Input Features", fontweight="bold")
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    return fig, axes


def view_predictions(y_test, predictions):
    """View the number of mistakes made by number of incorrectly classified digits.
    args:
        y_test : pd.series || np.array
        predictions: pd.DataFrame || np.array

    returns:
        (  fig: plt.Figure, ax: plt.Axes  )
    """
    df = pd.DataFrame()
    df["y_true"] = y_test
    df["y_pred"] = list(map(lambda x: np.argmax(x), predictions))

    # Discrepancies between the true and predicted digits.
    mistakes_made = df[df["y_true"] != df["y_pred"]]
    # The number of times the model made a mistake for each digit.
    mistake_counts = mistakes_made.groupby("y_true").size().sort_values(ascending=False)

    # Create the figure and the axes objects.
    fig, ax = plt.subplots(1, figsize=(8, 4))
    # Draw a bar graph on the axes.
    mistake_counts.plot.bar(color="royalblue", ax=ax)

    # Add titles to the axis drawn.
    ax.set_xlabel("Digit")
    ax.set_ylabel("Count")
    fig.suptitle(
        f"Mistakes Made by the Neural Network. {mistake_counts.sum()}/{len(y_test)}",
        fontweight="bold",
    )

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    return fig, ax
