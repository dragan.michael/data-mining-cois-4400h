import pandas as pd
import numpy as np
import tensorflow as tf


def get_splits() -> pd.DataFrame:
    """Get train and test splits
    args: None
    return: x_train, y_train, x_test, y_test
    """
    dataset = tf.keras.datasets.mnist
    (x_train, y_train), (x_test, y_test) = dataset.load_data()
    return x_train, y_train, x_test, y_test


def preprocess_raw(x_train, y_train, x_test, y_test):
    """Normalize the digits dataset
    args: x_train, y_train, x_test, y_test
    return: x_train, y_train, x_test, y_test
    """
    # Normalize
    x_train = tf.keras.utils.normalize(x_train, axis=1)
    x_test = tf.keras.utils.normalize(x_test, axis=1)

    return x_train, y_train, x_test, y_test
