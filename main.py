# %% Imports
import matplotlib.pyplot as plt  # Plotting Stuff
import pandas as pd  # A really great data science package for working with tables.
import tensorflow as tf
import toolkit
import numpy as np
from sklearn import preprocessing


def main():
    # CONSTANTS - model parameters
    OPTIMIZER = "adam"
    LOSS = "sparse_categorical_crossentropy"
    NUM_NODES_0 = 128  # Number of "neurons in the first hidden layer.
    NUM_NODES_1 = 128  # Number of "neurons" in the second hidden layer.
    METRICS = ["accuracy"]
    EPOCHS = 5  # Number of training iterations

    # Getting the raw training data (these are the handwritten digits).
    # NOTE: these are 2 dimensional arrays representing the brightness
    # of a pixel.
    x_train, y_train, x_test, y_test = toolkit.get_splits()

    # This step just normalizes the data so that it all fits between the
    # range of [0, 1]. This helps gradient descent operations.
    x_train, y_train, x_test, y_test = toolkit.preprocess_raw(
        x_train, y_train, x_test, y_test
    )

    # Viewing digits
    fig, axes = toolkit.view_digits(x_train, y_train)
    fig.savefig("./img/digits.png", format="png")
    model = toolkit.create_ann(NUM_NODES_0, NUM_NODES_1, OPTIMIZER, LOSS, METRICS)

    # Training the model
    model.fit(x_train, y_train, epochs=EPOCHS)

    # Evaluating the model
    test_loss, test_acc = model.evaluate(x_test, y_test)

    # Making a predictions
    predictions = model.predict(x_test)

    # View predictions
    fig01, ax = toolkit.view_predictions(y_test, predictions)
    fig01.savefig("./img/error_digits.png", format="png")

    # Display all visualizations and print out short datasdet descriptions
    print("\nDataset Split Sizes")
    print(f"Number of Training Instances: {len(y_train)}")
    print(f"Length of Testing Set: {len(y_test)}")
    print(f"Model Accuracy: {test_acc}")

    plt.show()


main()
