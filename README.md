# An Introduction to Data Mining and Machine Learning 
## Data Sesssions
### when? : 
* Fridays 10 - 3 pm
* come at your convenience, we know school can get stressful!
* complete the modules at your own pace
### about: what to expect
* sandbox lab approach
* hang out and discuss topics in data sciences
* help implement models and display cool visualizations
* a positive workspace that fosters empowerment over competition.
### outcomes: some things you can expect to learn 
* implement various kinds of neural networks using Keras and Tensorflow
* use sklearn to transform data, and build ensembles for classification problems
* learn popular data science libraries such as SciPy, Pandas, and Numpy.
* create beautiful visuals of data you've come across to share.
* deploy models to cloud services offered by AWS or Google Cloud.

You will not be assessed on anything you learn throughout this module. It is
entirely optional, let's just make this about building cool things :)! 

We're hoping to explore different avenues of data, so feel free to send us some
feedback.

## Ideas
* participating in a [Kaggle](https://www.kaggle.com/) competition
* submitting to r/dataisbeautiful [dataisbeautiful](https://www.reddit.com/r/dataisbeautiful/)

## Prerequisites
* Basic python is recommended but not required
* Good to knows: (basic input output, loops/enums, types, OOP or functional programming).

## Setup
I like using [venv](https://docs.python.org/3/tutorial/venv.html) to set up my 
virtual environment. It is lightweight and easy quick to get started with.

For something more sophisticated, check out [anaconda](https://www.anaconda.com/).
Conda will set up a stable collection of packages data scientists often use.
It's great when working with a team and or a larger enterprise.

cd into this root of this project and run the following command
in order to install this project's dependencies.

I'll help you get set up if you're having a lot of difficulty! No worries!
```bash
pip install -r requirements.txt
```
```bash
python main.py
```
If everything installed properly, the two plots stored in the ./img folder
should appear after the model has been trained.
![logs](./img/log_00.png)
![digits](./img/digits.png)
![error](./img/error_digits.png)

### What just happened?
You just built a neural network capable of recognizing handwritten digits!

For our first meeting, we will go through the code together and discuss!
